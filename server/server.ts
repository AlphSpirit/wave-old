import * as express from "express";
import * as http from "http";

const app = express();
const server = new http.Server(app);

app.get("/", (req, res) => {
    res.send("Allo");
});

server.listen(80, () => {
    console.log("Wave server started on port 80");
});
