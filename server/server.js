"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http = require("http");
const app = express();
const server = new http.Server(app);
app.get("/", (req, res) => {
    res.send("Allo");
});
server.listen(80, () => {
    console.log("Wave server started on port 80");
});
