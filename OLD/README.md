# Projet Wave

## Qu'est-ce que Wave?

Wave est une plateforme de musique qui met à la disposition des utilisateurs des outils afin de créer des radios temps réel ainsi que de monter des listes de lectures à partager avec tous. Mais wave ne s'arrête pas la, voici une liste non exhaustive de ce que wave peut faire pour vous:

* Radios temps réel avec synchronisation audio très précise
* La possibilitée de parler avec les autres auditeurs de la radio
* Écouter de la musique à l'aide du lecteur de musique intégré
* Listes de lectures à partager (ou à garder pour vous!)
* Visualisation audio en utilisant les dernières technologies web
* Beaucoup plus encore

## Quel est le but de wave?

Nous croyons que la musique fait bien plus qu'apporter de la satisfaction à l'écoute. Elle remplie nos journées, nous accompagne dans nos hauts et nos bas, est toujours là pour nous afin de nous remonter le moral, et, par dessus tout, rapelle de bons souvenirs. Votre première dance, votre premier baiser, votre super soirée entre amis, la musique s'en rapelle. Et vous aussi. Connectez-vous avec des personnes aussi passionnées que vous dans la musique que vous aimez tant.

## À propos de nous

Nous sommes Dominic Boulanger et Alexandre Desbiens, deux étudiants en programmation mais par dessus tout, deux passionnés de la musique, qui cherche à apporter la meilleure expérience qui soit en plateforme musicale.

## Technologies

Nous utilisons JSDoc pour la documentation. Vous pouvez trouver le template ici : [jaguarjs-jsdoc](https://github.com/davidshimjs/jaguarjs-jsdoc)


Back-end

* [Node.js](https://nodejs.org/en/) - Engin du serveur web
* [MySQL](https://github.com/mysqljs/mysql) - Driver MySQL pour Node.js
* [Mochajs](https://mochajs.org/) - Framework de test pour Node.js
* [Expressjs](http://expressjs.com/) - Middleware Node pour serveur web
* [Socket.io](http://socket.io/) - Middleware Node pour communication Web Sockets
* [Sequelize](http://docs.sequelizejs.com/) - ORM MySQL

Front-end

* [Backbone.js](http://backbonejs.org/) - Framework MVC Web
* [Underscore.js](http://underscorejs.org/) - Librairie Utilitaire
* [jQuery](https://jquery.com/) - Framework DOM Manipulation
* [Bootstrap 4](http://getbootstrap.com/) - Framework Front-end
* [Socket.io](http://socket.io/) - Web Sockets Javascript
* [Marionettejs](http://marionettejs.com/) - Simplification de Backbone
* [Hammerjs](http://hammerjs.github.io/) - Librairie Front-end pour le touch