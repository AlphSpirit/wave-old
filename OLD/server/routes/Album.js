module.exports = function(models) {

    var routes = {};

    routes.getAlbum = function(req, res) {
        models.Album.find({
            where: {
                id: req.params.id
            }
        }).then(function(album) {
            res.status(200).send(album);
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.getAllAlbums = function(req, res) {
        models.Album.findAll({
        }).then(function(albums) {
            res.status(200).send(albums);
        }).catch(function(err) {
            res.status(500).send(err);
        });
    };

    routes.createAlbum = function(req, res) {
        models.Album.create({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture,
            releaseDate: req.body.releaseDate
        }).then(function() {
            res.status(200).send({ message: "Album successfully created." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);  
        });
    };

    routes.updateAlbum = function(req, res) {
        models.Album.update({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture,
            releaseDate: req.body.releaseDate
        }).then(function() {
            res.status(200).send({ message: "Album successfully updated." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.deleteAlbum = function(req, res) {
        models.Album.destroy({
            where: {
                id: req.params.id
            }
        }).then(function() {
            res.status(200).send({ message: "Album successfully deleted." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    }

    return routes;

};