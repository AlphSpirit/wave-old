module.exports = function(models) {

    var routes = {};

    routes.getSong = function(req, res) {
        models.Song.find({
            where: {
                id: req.params.id
            }
        }).then(function(song) {
            res.status(200).send(song);
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.getAllSongs = function(req, res) {
        models.Song.findAll({
        }).then(function(songs) {
            res.status(200).send(songs);
        }).catch(function(err) {
            res.status(500).send(err);
        });
    };

    routes.createSong = function(req, res) {
        models.Song.create({
            name: req.body.name,
            path: req.body.path,
            duration: req.body.duration
        }).then(function() {
            res.status(200).send({ message: "Song successfully created." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);  
        });
    };

    routes.updateSong = function(req, res) {
        models.Song.update({
            name: req.body.name,
            path: req.body.path,
            duration: req.body.duration
        }).then(function() {
            res.status(200).send({ message: "Song successfully updated." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.deleteSong = function(req, res) {
        models.Song.destroy({
            where: {
                id: req.params.id
            }
        }).then(function() {
            res.status(200).send({ message: "Song successfully deleted." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    }

    return routes;

};