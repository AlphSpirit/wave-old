module.exports = function(models) {

    var routes = {};

    routes.getPlaylist = function(req, res) {
        models.Playlist.find({
            where: {
                id: req.params.id
            }
        }).then(function(playlist) {
            res.status(200).send(playlist);
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.getAllPlaylists = function(req, res) {
        models.Playlist.findAll({
        }).then(function(playlists) {
            res.status(200).send(playlists);
        }).catch(function(err) {
            res.status(500).send(err);
        });
    };

    routes.createPlaylist = function(req, res) {
        models.Playlist.create({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture,
            public: req.body.public,
            UserId: req.body.userId
        }).then(function() {
            res.status(200).send({ message: "Playlist successfully created." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);  
        });
    };

    routes.updatePlaylist = function(req, res) {
        models.Playlist.update({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture,
            public: req.body.public
        }).then(function() {
            res.status(200).send({ message: "Playlist successfully updated." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.deletePlaylist = function(req, res) {
        models.Playlist.destroy({
            where: {
                id: req.params.id
            }
        }).then(function() {
            res.status(200).send({ message: "Playlist successfully deleted." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    }

    return routes;

};