module.exports = function(app, models) {

    var userRoutes = require("./User.js")(models);
    app.get("/api/user/:id", userRoutes.getUser);
    app.get("/api/user", userRoutes.getAllUsers);
    app.post("/api/user", userRoutes.createUser);
    app.post("/api/user/:id", userRoutes.updateUser);
    app.post("/api/user/:id/credentials", userRoutes.updateUserCredentials);
    app.post("/api/user/delete/:id", userRoutes.deleteUser);

    var playlistRoutes = require("./Playlist.js")(models);
    app.get("/api/playlist/:id", playlistRoutes.getPlaylist);
    app.get("/api/playlist", playlistRoutes.getAllPlaylists);
    app.post("/api/playlist", playlistRoutes.createPlaylist);
    app.post("/api/playlist/:id", playlistRoutes.updatePlaylist);
    app.post("/api/playlist/delete/:id", playlistRoutes.deletePlaylist);

    var songRoutes = require("./Song.js")(models);
    app.get("/api/song/:id", songRoutes.getSong);
    app.get("/api/song", songRoutes.getAllSongs);
    app.post("/api/song", songRoutes.createSong);
    app.post("/api/song/:id", songRoutes.updateSong);
    app.post("/api/song/delete/:id", songRoutes.deleteSong);

    var artistRoutes = require("./Artist.js")(models);
    app.get("/api/artist/:id", artistRoutes.getArtist);
    app.get("/api/artist", artistRoutes.getAllArtists);
    app.post("/api/artist", artistRoutes.createArtist);
    app.post("/api/artist/:id", artistRoutes.updateArtist);
    app.post("/api/artist/delete/:id", artistRoutes.deleteArtist);

    var albumRoutes = require("./Album.js")(models);
    app.get("/api/album/:id", albumRoutes.getAlbum);
    app.get("/api/album", albumRoutes.getAllAlbums);
    app.post("/api/album", albumRoutes.createAlbum);
    app.post("/api/album/:id", albumRoutes.updateAlbum);
    app.post("/api/album/delete/:id", albumRoutes.deleteAlbum);

};