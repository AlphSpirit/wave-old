var bcrypt = require("bcrypt-nodejs");
module.exports = function(models) {

    var routes = {};

    routes.getUser = function(req, res) {
        models.User.find({
            where: {
                id: req.params.id
            },
            attributes: {
                exclude: ['password']
            }
        }).then(function(user) {
            res.status(200).send(user);
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.getAllUsers = function(req, res) {
        models.User.findAll({
            attributes: {
                exclude: ['password']
            }
        }).then(function(users) {
            res.status(200).send(users);
        }).catch(function(err) {
            res.status(500).send(err);
        });
    };

    routes.createUser = function(req, res) {
        models.User.create({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password),
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            picture: req.body.picture,
            about: req.body.about
        }).then(function() {
            res.status(200).send({ message: "User successfully created." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);  
        });
    };

    routes.updateUser = function(req, res) {
        models.User.update({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            picture: req.body.picture,
            about: req.body.about
        }).then(function() {
            res.status(200).send({ message: "User successfully updated." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.updateUserCredentials = function(req, res) {
        models.User.update({
            password: bcrypt.hashSync(req.body.password)
        }).then(function() {
            res.status(200).send({ message: "User successfully updated." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.deleteUser = function(req, res) {
        models.User.destroy({
            where: {
                id: req.params.id
            }
        }).then(function() {
            res.status(200).send({ message: "User successfully deleted." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    }

    return routes;

};