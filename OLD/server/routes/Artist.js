module.exports = function(models) {

    var routes = {};

    routes.getArtist = function(req, res) {
        models.Artist.find({
            where: {
                id: req.params.id
            }
        }).then(function(artist) {
            res.status(200).send(artist);
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.getAllArtists = function(req, res) {
        models.Artist.findAll({
        }).then(function(artists) {
            res.status(200).send(artists);
        }).catch(function(err) {
            res.status(500).send(err);
        });
    };

    routes.createArtist = function(req, res) {
        models.Artist.create({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture
        }).then(function() {
            res.status(200).send({ message: "Artist successfully created." });
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);  
        });
    };

    routes.updateArtist = function(req, res) {
        models.Artist.update({
            name: req.body.name,
            description: req.body.description,
            picture: req.body.picture
        }).then(function() {
            res.status(200).send({ message: "Artist successfully updated." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    };

    routes.deleteArtist = function(req, res) {
        models.Artist.destroy({
            where: {
                id: req.params.id
            }
        }).then(function() {
            res.status(200).send({ message: "Artist successfully deleted." })
        }).catch(function(err) {
            console.log(err);
            res.status(500).send(err);
        });
    }

    return routes;

};