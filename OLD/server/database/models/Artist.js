module.exports = function(sequelize, DataTypes) {
    var Artist = sequelize.define("Artist", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: DataTypes.STRING },
        description: { type: DataTypes.STRING },
        picture: { type: DataTypes.STRING }
    }, {
        classMethods: {
            associate: function(models) {
                Artist.belongsToMany(models.Song, { through: 'SongArtist' })
                Artist.belongsToMany(models.Album, { through: 'ArtistAlbum' })
            }
        }
    });
    return Artist;
};