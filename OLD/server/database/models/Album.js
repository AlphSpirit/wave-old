module.exports = function(sequelize, DataTypes) {
    var Album = sequelize.define("Album", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: DataTypes.STRING },
        description: { type: DataTypes.STRING },
        picture: { type: DataTypes.STRING },
        releaseDate: { type: DataTypes.DATE }
    }, {
        classMethods: {
            associate: function(models) {
                Album.belongsToMany(models.Song, { through: 'AlbumSong' })
                Album.belongsToMany(models.Artist, { through: 'ArtistAlbum' })
            }
        }
    });
    return Album;
};