module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        username: { type: DataTypes.STRING, unique: true },
        password: { type: DataTypes.STRING },
        firstName: { type: DataTypes.STRING },
        lastName: { type: DataTypes.STRING },
        email: { type: DataTypes.STRING },
        picture: { type: DataTypes.STRING },
        about: { type: DataTypes.STRING }
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Playlist)
            }
        }
    });
    return User;
};