module.exports = function(sequelize, DataTypes) {
    var Broadcast = sequelize.define("Broadcast", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: DataTypes.STRING },
        description: { type: DataTypes.STRING },
        picture: { type: DataTypes.STRING }
    }, {
        classMethods: {
            associate: function(models) {
                Broadcast.belongsTo(models.User)
            }
        }
    });
    return Broadcast;
};