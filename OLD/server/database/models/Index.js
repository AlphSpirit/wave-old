const Sequelize = require("sequelize");
const sequelize = new Sequelize("wave_test", "root", "root");

var database = {};

// Retrieve models
var userModel = sequelize.import("./User.js");
database.User = userModel;
var broadcastModel = sequelize.import("./Broadcast.js");
database.Broadcast = broadcastModel;
var playlistModel = sequelize.import("./Playlist.js");
database.Playlist = playlistModel;
var songModel = sequelize.import("./Song.js");
database.Song = songModel;
var artistModel = sequelize.import("./Artist.js");
database.Artist = artistModel;
var albumModel = sequelize.import("./Album.js");
database.Album = albumModel;

// Call association methods
database.User.associate(database);
database.Broadcast.associate(database);
database.Playlist.associate(database);
database.Song.associate(database);
database.Artist.associate(database);
database.Album.associate(database);

database.sequelize = sequelize;
database.Sequelize = Sequelize;

module.exports = database;