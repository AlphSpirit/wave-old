module.exports = function(sequelize, DataTypes) {
    var Song = sequelize.define("Song", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: DataTypes.STRING },
        path: { type: DataTypes.STRING },
        duration: { type: DataTypes.INTEGER }
    }, {
        classMethods: {
            associate: function(models) {
                Song.belongsToMany(models.Playlist, { through: 'PlaylistSong' })
                Song.belongsToMany(models.Artist, { through: 'SongArtist' })
                Song.belongsToMany(models.Album, { through: 'AlbumSong' })
            }
        }
    });
    return Song;
};