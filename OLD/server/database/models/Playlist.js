module.exports = function(sequelize, DataTypes) {
    var Playlist = sequelize.define("Playlist", {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: DataTypes.STRING },
        description: { type: DataTypes.STRING },
        picture: { type: DataTypes.STRING },
        public: { type: DataTypes.BOOLEAN }
    }, {
        classMethods: {
            associate: function(models) {
                Playlist.belongsTo(models.User)
                Playlist.belongsToMany(models.Song, { through: 'PlaylistSong' })
            }
        }
    });
    return Playlist;
};