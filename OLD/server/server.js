var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var models = require("./database/models");
var setupRoutes = require("./routes");
var server = null;
const SERVER_PORT = 2535;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());

app.disable("etag");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

function startServer() {
    server = app.listen(SERVER_PORT, "0.0.0.0", function() {
        console.log("Server started listening on port " + SERVER_PORT.toString());
    });
}

models.sequelize.sync()
    .then(setupRoutes(app, models))
    .then(startServer);