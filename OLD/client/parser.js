//@ts-check
var INDENTATION = 4;

var fs = require("fs");

var fileName = process.argv[2];

fs.readFile(fileName, "utf-8", function(error, data) {

    if (error) {
        console.log("ERROR");
        console.log(error);
        return;
    }

    // Split lines and calculate depth level
    var split = data.split("\n");
    var dataLines = [];
    for (var line of split) {
        var line = trimEnd(line);
        var level = 0;
        while (line.startsWith("  ")) {
            level++;
            line = line.substring(INDENTATION, line.length);
        }
        dataLines.push({
            line: line,
            level: level
        });
    }

    // Construct element tree
    var currentNode = parseNode(dataLines[0]);
    var rootNode = {
        level: -1,
        element: null,
        parent: null,
        nodes: [currentNode]
    };

    currentNode.parent = rootNode;
    dataLines.splice(0, 1);
    for (var index = 0; index < dataLines.length; index++) {
        var dataLine = dataLines[index];
        var node = null;
        if (dataLine.line) {
            if (dataLine.level == currentNode.level + 1) {
                node = parseNode(dataLine, currentNode);
            } else if (dataLine.level == currentNode.level) {
                currentNode = currentNode.parent;
                node = parseNode(dataLine, currentNode);
            } else if (dataLine.level < currentNode.level) {
                var diff = currentNode.level - dataLine.level + 1;
                while (diff > 0) {
                    currentNode = currentNode.parent;
                    diff--;
                }
                node = parseNode(dataLine, currentNode);
            }
        }
        if (node) {
            currentNode.nodes.push(node);
            currentNode = node;
            if (node.openMultilineValue) {
                for (var index2 = index + 1; index2 < dataLines.length; index2++) {
                    var dataLine = dataLines[index2];
                    if (dataLine.level == node.level && dataLine.line == "}") {
                        dataLines.splice(index, index2 - index);
                        break;
                    }
                    node.content.push(dataLine);
                }
            }
        }
    }

    // Output html
    var output = outputNode(rootNode);
    if (rootNode.nodes[0].element == "html") {
        output = "<!DOCTYPE html>\n" + output;
    }
    fs.writeFile(fileName.replace(".shtml", ".html"), output, function(error) {
        if (error) {
            console.log("ERROR");
            console.log(error);
        } else {
            console.log("Compiling done.");
        }
    });

});

/** @param {string} str */
function trimEnd(str) {
    return str.replace(/\s*$/, "");
}

function parseNode(dataLine, parent) {

    // Check if text node
    if (/^".*"$/.test(dataLine.line)) {
        var obj = {
            level: dataLine.level,
            element: null,
            id: null,
            classes: [],
            attributes: [],
            content: [dataLine.line.substring(1, dataLine.line.length - 1)],
            nodes: [],
            parent: parent || null
        };
        return obj;
    }

    var id = null;
    var classes = [];
    var attributes = [];
    var content = [];
    var openMultilineValue = false;

    // Check if a multiline value is opened
    if (dataLine.line.endsWith("{")) {
        openMultilineValue = true;
        dataLine.line = dataLine.line.substring(0, dataLine.line.length - 1);
    }

    // Extract attributes
    var r = /\[.*\]/.exec(dataLine.line);
    while (r) {
        var split = [];
        var start = 1;
        var inString = false;
        for (var i = 1; i < r[0].length - 1; i++) {
            if (r[0][i] == '"') {
                inString = !inString;
            } else if (r[0][i] == ' ' && !inString) {
                split.push(r[0].substring(start, i));
                start = i + 1;
            }
        }
        split.push(r[0].substring(start, i));
        for (var attribute of split) {
            var equalIndex = attribute.indexOf("=");
            if (equalIndex != -1) {
                var subSplit = [
                    attribute.substring(0, equalIndex),
                    attribute.substring(equalIndex + 1, attribute.length)
                ];
                if (subSplit[1].startsWith('"') && subSplit[1].endsWith('"')) {
                    subSplit[1] = subSplit[1].substring(1, subSplit[1].length - 1);
                }
                attributes.push({
                    name: subSplit[0],
                    value: subSplit[1].replace("'", "&#39;")
                });
            } else {
                attributes.push({
                    name: attribute,
                    value: null
                });
            }
        }
        dataLine.line = dataLine.line.substring(0, r.index) + dataLine.line.substring(r.index + r[0].length, dataLine.line.length);
        r = /\[."\]/.exec(dataLine.line);
    }

    // Extract content
    r = /\{.*\}/.exec(dataLine.line);
    while (r) {
        content.push(r[0].substring(1, r[0].length - 1));
        dataLine.line = dataLine.line.substring(0, r.index) + dataLine.line.substring(r.index + r[0].length, dataLine.line.length);
        r = /\{."\}/.exec(dataLine.line);
    }

    // Extract id
    r = /\#[a-zA-Z0-9-]+/.exec(dataLine.line);
    while (r) {
        id = r[0].substring(1, r[0].length)
        dataLine.line = dataLine.line.substring(0, r.index) + dataLine.line.substring(r.index + r[0].length, dataLine.line.length);
        r = /\#[a-zA-Z0-9-]+/.exec(dataLine.line);
    }

    // Extract classes
    r = /\.[a-zA-Z0-9-]+/.exec(dataLine.line);
    while (r) {
        classes.push(r[0].substring(1, r[0].length));
        dataLine.line = dataLine.line.substring(0, r.index) + dataLine.line.substring(r.index + r[0].length, dataLine.line.length);
        r = /\.[a-zA-Z0-9-]+/.exec(dataLine.line);
    }

    var node = {
        level: dataLine.level,
        element: dataLine.line ? dataLine.line : "div",
        id: id,
        classes: classes,
        attributes: attributes,
        content: content,
        nodes: [],
        parent: parent || null,
        openMultilineValue: openMultilineValue
    };
    return node;

}

function outputNode(node) {
    var str = "";
    if (node.level == -1) {
        for (var child of node.nodes) {
            str += outputNode(child);
        }
        return str;
    }
    for (var i = 0; i < node.level; i++) {
        str += "    ";
    }
    if (node.element) {
        str += "<" + node.element;
        if (node.id) {
            str += " id='" + node.id + "'";
        }
        if (node.classes.length > 0) {
            str += " class='";
            for (var className of node.classes) {
                str += className + " ";
            }
            str = str.substring(0, str.length - 1);
            str += "'";
        }
        if (node.attributes.length > 0) {
            for (var attribute of node.attributes) {
                str += " " + attribute.name + "='" + (attribute.value || "") + "'";
            }
        }
        str += ">";
    }
    if (node.content.length > 0) {
        //str += node.content;
        if (node.content.length > 1) {
            str += "\n";
        }
        for (var c of node.content) {
            if (node.content.length > 1 && c.level) {
                if (c.level) {
                    for (var i = 0; i < c.level; i++) {
                        str += "    ";
                    }
                } else {
                    for (var i = 0; i < node.level; i++) {
                        str += "    ";
                    }
                }
            }
            if (c.level) {
                str += c.line;
            } else {
                str += c;
            }
            if (node.content.length > 1) {
                str += "\n";
            }
        }
    }
    if (node.nodes.length > 0) {
        if (node.nodes.length == 1 && !node.nodes[0].element && node.nodes[0].content.length == 1) {
            str += node.nodes[0].content;
        } else {
            str += "\n";
            for (var childNode of node.nodes) {
                str += outputNode(childNode);
            }
            for (var i = 0; i < node.level; i++) {
                str += "    ";
            }
        }
    }
    if (node.element && !isAutoClosing(node.element)) {
        if (node.content.length > 1) {
            for (var i = 0; i < node.level; i++) {
                str += "    ";
            }
        }
        str += "</" + node.element + ">";
    }
    str += "\n";
    return str;
}

function isAutoClosing(element) {
    return /(meta|link|br|hr|input)/g.test(element);
}
