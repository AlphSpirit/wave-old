var fs = require("fs");
var exec = require('child_process').exec;

var DEBUG_MODE = true;

var folderDist = "dist\\www";
var foldersToCreate = [
    "dist",
    folderDist
];
var foldersToCopy = [
    "fonts",
    "img",
    "media"
];
var foldersToIgnore = [
    "dist",
    ".git",
    "node_modules",
    "fonts",
    "img",
    "concept",
    "doc",
    ".vscode"
];
var filesToIgnore = [
    "deploy.sh",
    "deploy.node.js"
];

deleteDirectoryRecursive(folderDist);

for (var folder of foldersToCreate) {
    createDirectory(folder);
}

if (DEBUG_MODE) {
    console.log("Minifying...");
} else {
    process.stdout.write("Deploying");
}
minify(".");
console.log("");
console.log("Finalising");

for (var folder of foldersToCopy) {
    var toExecute = "xcopy " + folder + " " + folderDist + "\\" + folder + "\\";
    if (DEBUG_MODE) {
        console.log(toExecute);
    }
    exec(toExecute);
}

function minify(file) {
    if (!isDirectory(file)) {
        for (var ignore of filesToIgnore) {
            if (file.endsWith("\\" + ignore)) {
                return;
            }
        }
        var command;
        var flags = "";
        if (file.endsWith(".js")) {
            if (file.endsWith(".min.js")) {
                exec("cp " + file + " " + folderDist + "\\" + file);
                return;
            }
            command = "uglifyjs";
            flags = "-m"
        } else if (file.endsWith(".html")) {
            command = "html-minifier";
            flags = "--collapse-whitespace --remove-comments"
        } else if (file.endsWith(".css")) {
            command = "cleancss";
            flags = "--skip-rebase";
        }
        if (command) {
            var toExecute = command + " " + file + " -o " + folderDist + "\\" + file + " " + flags;
            if (DEBUG_MODE) {
                console.log(toExecute);
            } else {
                process.stdout.write(".");
            }
            exec(toExecute, function(error, stdout, stderr) {
                if (error) {
                    console.log("Error with command:");
                    console.log(error.cmd);
                    console.log(stderr);
                }
                //console.log(err);
                //console.log(stderr);
            });
        }
    } else {
        for (var ignore of foldersToIgnore) {
            if (file.endsWith("\\" + ignore)) {
                return;
            }
        }
        createDirectory(folderDist + "\\" + file);
        var files = fs.readdirSync(file);
        for (var innerFile of files) {
            minify(file + "\\" + innerFile);
        }
    }
}

function isDirectory(path) {
    return fs.lstatSync(path).isDirectory();
}

function createDirectory(folder) {
    if (!fs.existsSync(folder)) {
        if (DEBUG_MODE) {
            console.log("mfdir " + folder);
        }
        fs.mkdirSync(folder);
    }
    return true;
}

// http://stackoverflow.com/questions/18052762/remove-directory-which-is-not-empty
function deleteDirectoryRecursive(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "\\" + file;
            if (isDirectory(curPath)) {
                deleteDirectoryRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
