define([
    "backbone.marionette",
    "views/viewGlobalMenu",
    "views/viewIntro",
    "views/viewLogin",
    "views/viewRegister",
    "views/viewTest"
], function(Mn, ViewGlobalMenu, ViewIntro, ViewLogin, ViewRegister, ViewTest) {

    var Router = Mn.AppRouter.extend({

        routes: {
            "test": "test",
            "intro": "intro",
            "login": "login",
            "register": "register",
            "*path": "test"
        },

        init: function() {
            Backbone.history.start();
        },

        test: function() {
            this.loadGlobalMenuView(new ViewTest());
        },

        intro: function() {
            this.loadGlobalMenuView(new ViewIntro());
        },

        login: function() {
            this.loadGlobalMenuView(new ViewLogin());
        },

        register: function() {
            this.loadGlobalMenuView(new ViewRegister());
        },

        loadGlobalMenuView: function(view) {
            if (!(this.currentView instanceof ViewGlobalMenu)) {
                this.currentView = new ViewGlobalMenu();
                this.currentView.render();
            }
            this.currentView.loadRegionView(view);
        }

    });

    return new Router;

})
