define(["jquery"], function($) {
    var AirRipple = {
        init: function(selector, options) {
            var that = this;
            $(document).on("click", selector, function(e) {
                that.createRipple(this, e);
            });
        },
        createRipple: function(element, event) {
            element = $(element);
            var ripple = $("<div></div>");
            var rippleX = event.pageX - element.offset().left;
            var rippleY = event.pageY - element.offset().top;
            var rippleColor;
            if (element.hasClass("ripple-dark")) {
                rippleColor = "rgba(0, 0, 0, 0.1)";
            } else {
                rippleColor = "rgba(255, 255, 255, 0.2)";
            }
            element.css("position", "relative");
            ripple.css("width", "100%")
                .css("height", "100%")
                .css("position", "absolute")
                .css("left", "0px")
                .css("top", "0px")
                .css("border-radius", element.css("border-radius"))
                .css("background", "radial-gradient(circle at " + rippleX + "px " + rippleY + "px, transparent, " + rippleColor + " 0%, transparent 1%)")
                .css("transition", "background 1s ease");
            var ripplePercent = 0;
            var rippleOpacity = 1;
            var interval = setInterval(function() {
                if (ripplePercent < 100) {
                    ripplePercent += 6;
                    ripple.css("background", "radial-gradient(circle at " + rippleX + "px " + rippleY + "px, " + rippleColor + ", " + rippleColor + " " + ripplePercent + "%, transparent " + (ripplePercent + 5) + "%)")
                } else if (rippleOpacity > 0) {
                    rippleOpacity -= 0.05;
                    ripple.css("opacity", rippleOpacity);
                } else {
                    clearInterval(interval);
                    ripple.remove();
                }
            }, 20);
            element.append(ripple);
        }
    }
    return AirRipple;
})
