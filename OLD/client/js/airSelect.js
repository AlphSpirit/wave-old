(function($) {
    $.fn.airSelect = function() {
        $.each(this, function(key, original) {
            var customSelect = $("<div></div>");
            $(original).before(customSelect);
            customSelect.attr("style", $(original).attr("style"));
            customSelect.attr("class", $(original).attr("class"));
            $(original).hide();
            customSelect.addClass("airSelect");
            var root = $("<div></div>");
            root.addClass("airSelectRoot");
            root.html($(original).find("option:selected").text());
            customSelect.append(root);
            var options = $("<div></div>");
            options.addClass("airSelectOptions");
            $.each($(original).find("option"), function(key, value) {
                if (!$(value).attr("hidden")) {
                    var option = $("<div>" + $(value).text() + "</div>");
                    option.addClass("airSelectOption");
                    option.attr("value", $(value).attr("value"));
                    option.on("click", function() {
                        root.html(option.html());
                        root.trigger("click");
                        $(original).val(option.attr("value"));
                    });
                    options.append(option);
                }
            });
            options.hide();
            root.on("click", function() {
                if (root.hasClass("opening") || root.hasClass("closing")) {
                    return;
                }
                if (root.hasClass("open")) {
                    root.addClass("closing").removeClass("open");
                    options.find(".airSelectOption").slideUp(400, function() {
                        options.hide();
                        root.removeClass("closing");
                    })
                } else {
                    root.addClass("open opening");
                    options.css("top", customSelect.height()).show().find(".airSelectOption").hide().slideDown(300, function() {
                        root.removeClass("opening");
                    });
                }
            });
            customSelect.append(options);
        });
    }
})(jQuery);
