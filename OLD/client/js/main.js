requirejs.config({
    paths: {
        "jquery": "../lib/jquery-3.2.1",
        "underscore": "../lib/underscore",
        "backbone": "../lib/backbone",
        "backbone.marionette": "../lib/backbone.marionette",
        "backbone.radio": "../lib/backbone.radio",
        "text": "../lib/text"
    },
    shim: {
        "backbone": {
            deps: ["underscore"]
        },
        "backbone.marionette": {
            deps: ["backbone", "backbone.radio"]
        },
        "backbone.radio": {
            deps: ["backbone"]
        },
        "airSelect": {
            deps: ["jquery"]
        }
    }
});

requirejs([
    "router",
    "airRipple",
    "airSelect"
], function(Router, AirRipple) {

    AirRipple.init(".ripple");
    Router.init();

})
