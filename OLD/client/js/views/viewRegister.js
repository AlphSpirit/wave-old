define([
    "underscore",
    "backbone.marionette",
    "text!templates/templateRegister.html"
], function(_, Mn, templateRegister) {

    var ViewRegister = Mn.View.extend({

        template: _.template(templateRegister),

        onAttach: function() {
        },

        events: {
            "click #btnRegister": "register"
        },

        register: function() {
            var username = $("#txtUsername").val();
            var email = $("#txtEmail").val();
            var password = $("#txtPassword").val();
            $.ajax({
                url: "",
                data: {
                    username: username,
                    email: email,
                    password: password
                }
            }).done(function() {
                Backbone.history.navigate("test", {trigger: true});
            }).fail(function() {
                // ERROR
            });
        }

    });

    return ViewRegister;

});
