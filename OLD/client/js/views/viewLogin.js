define([
    "underscore",
    "backbone.marionette",
    "text!templates/templateLogin.html"
], function(_, Mn, templateLogin) {

    var ViewLogin = Mn.View.extend({

        template: _.template(templateLogin),

        onAttach: function() {
        },

        events: {
            "click #btnLogin": "login"
        },

        login: function() {
            var email = $("#txtEmail").val();
            var password = $("#txtPassword").val();
            $.ajax({
                url: "",
                data: {
                    email: email,
                    password: password
                }
            }).done(function() {
                Backbone.history.navigate("test", {trigger: true});
            }).fail(function() {
                // ERROR
            });
        }

    });

    return ViewLogin;

});
