define([
    "underscore",
    "backbone.marionette",
    "text!templates/templateTest.html"
], function(_, Mn, templateTest) {

    var ViewTest = Mn.View.extend({

        template: _.template(templateTest),

        onAttach: function() {
            $("select").airSelect();
        }

    });

    return ViewTest;

});
