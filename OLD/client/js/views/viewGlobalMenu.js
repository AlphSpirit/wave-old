define([
    "underscore",
    "backbone.marionette",
    "text!templates/templateGlobalMenu.html"
], function(_, Mn, templateGlobalMenu) {
    
    var ViewGlobalMenu = Mn.View.extend({
        
        el: "#divMain",
        template: _.template(templateGlobalMenu),

        regions: {
            regionMain: "#divRegionMain"
        },
        
        loadRegionView: function(view) {
            var currentView = this.getChildView("regionMain");
            var delay = 0;
            if (currentView && currentView.dispose) {
                delay = currentView.dispose();
            }
            setTimeout((function() {
                view.parentView = this;
                this.showChildView("regionMain", view);
                unwrapView(this.getChildView("regionMain"));
            }).bind(this), delay);
        },
        
    });
    
    return ViewGlobalMenu;
     
});
