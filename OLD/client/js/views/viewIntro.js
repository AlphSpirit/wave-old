define([
    "underscore",
    "backbone.marionette",
    "text!templates/templateIntro.html"
], function(_, Mn, templateIntro) {

    var ViewIntro = Mn.View.extend({

        template: _.template(templateIntro),

        onAttach: function() {
        }

    });

    return ViewIntro;

});
